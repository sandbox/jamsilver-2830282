Field attach form selective
===========================
A low-level utility module which facilitates entity edit forms which do not attach field forms by default, but rather allow the user to select which fields to edit. This is useful when an entity type has many fields but where individual entities populate relatively few of them.


Usage
-----
This module provides no UI overrides or configurability on its own. It is designed to be used by other modules either for their own entity types, or to override the edit forms of existing entity types.

This module provides a single function:

    field_attach_form_selective()

Which should be used as an in-place replacement of core's:

    field_attach_form()

The function signatures are identical.
