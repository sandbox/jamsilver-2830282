<?php
/**
 * @file
 * Our re-implementation of field_attach_form and dependent functions.
 */

/**
 * Drop-in replacement for core's field_attach_form().
 *
 * @see field_attach_form()
 * @see field_attach_form_selective()
 */
function _field_attach_form_selective($entity_type, $entity, &$form, &$form_state, $langcode = NULL, $options = array()) {
  // Validate $options since this is a new parameter added after Drupal 7 was
  // released.
  $options = is_array($options) ? $options : array();

  // Set #parents to 'top-level' by default.
  $form += array('#parents' => array());

  // If no language is provided use the default site language.
  $options['language'] = field_valid_language($langcode);
  $form += (array) _field_attach_form_selective_field_invoke_default('form', $entity_type, $entity, $form, $form_state, $options);

  // Add custom weight handling.
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $form['#pre_render'][] = '_field_extra_fields_pre_render';
  $form['#entity_type'] = $entity_type;
  $form['#bundle'] = $bundle;

  // Let other modules make changes to the form.
  // Avoid module_invoke_all() to let parameters be taken by reference.
  foreach (module_implements('field_attach_form') as $module) {
    $function = $module . '_field_attach_form';
    $function($entity_type, $entity, $form, $form_state, $langcode);
  }
}

/**
 * Drop-in replacement for core's field_invoke_default().
 *
 * @see field_invoke_default()
 */
function _field_attach_form_selective_field_invoke_default($op, $entity_type, $entity, &$a = NULL, &$b = NULL, $options = array()) {
  $options['default'] = TRUE;
  return _field_attach_form_selective_field_invoke($op, $entity_type, $entity, $a, $b, $options);
}

/**
 * Drop-in replacement for core's _field_invoke().
 *
 * @see _field_invoke()
 */
function _field_attach_form_selective_field_invoke($op, $entity_type, $entity, &$a = NULL, &$b = NULL, $options = array()) {
  // Merge default options.
  $default_options = array(
    'default' => FALSE,
    'deleted' => FALSE,
    'language' => NULL,
  );
  $options += $default_options;

  // Determine the list of instances to iterate on.
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);
  $instances = _field_invoke_get_instances($entity_type, $bundle, $options);

  $restricted_instances = $instances;
  if (in_array($op, array('form', 'validate', 'submit'), TRUE)) {
    $restricted_instances = _field_attach_form_selective_limit_instances($instances, $b, $entity);
  }
  // Insert the selector.
  if ($op === 'form') {
    _field_attach_form_selective_field_selector($a, $b, $entity_type, $entity, $instances, $restricted_instances);
  }

  // Iterate through the instances and collect results.
  $return = array();
  foreach ($restricted_instances as $instance) {
    // field_info_field() is not available for deleted fields, so use
    // field_info_field_by_id().
    $field = field_info_field_by_id($instance['field_id']);
    $field_name = $field['field_name'];
    $function = $options['default'] ? 'field_default_' . $op : $field['module'] . '_field_' . $op;
    if (function_exists($function)) {
      // Determine the list of languages to iterate on.
      $available_languages = field_available_languages($entity_type, $field);
      $languages = _field_language_suggestion($available_languages, $options['language'], $field_name);

      foreach ($languages as $langcode) {
        $items = isset($entity->{$field_name}[$langcode]) ? $entity->{$field_name}[$langcode] : array();
        $result = $function($entity_type, $entity, $field, $instance, $langcode, $items, $a, $b);
        if (isset($result)) {
          // For hooks with array results, we merge results together.
          // For hooks with scalar results, we collect results in an array.
          if (is_array($result)) {
            $return = array_merge($return, $result);
          }
          else {
            $return[] = $result;
          }
        }

        // Populate $items back in the field values, but avoid replacing missing
        // fields with an empty array (those are not equivalent on update).
        if ($items !== array() || isset($entity->{$field_name}[$langcode])) {
          $entity->{$field_name}[$langcode] = $items;
        }
      }
    }
  }

  return $return;
}

/**
 * Helper which restricts the instances to those we should render on the form.
 *
 * That is to say: those for which the entity has a value, or is required, or
 * that the user has specifically requested to be on the form.
 */
function _field_attach_form_selective_limit_instances($instances, &$form_state, $entity) {
  $restricted_instances = array();
  foreach ($instances as $key => $instance) {
    $field_name = $instance['field_name'];
    if (!empty($entity->{$field_name}) || !empty($instance['required']) || isset($form_state['field_attach_form_selective_fields'][$field_name])) {
      $restricted_instances[$key] = $instance;
    }
  }
  return $restricted_instances;
}

/**
 * Add the field selector widget to a form.
 */
function _field_attach_form_selective_field_selector(&$form, &$form_state, $entity_type, $entity, $all_instances, $restricted_instances) {
  $options = array();
  foreach ($all_instances as $key => $instance) {
    if (!isset($restricted_instances[$key])) {
      $options[$instance['field_name']] = $instance['label'];
    }
  }
  if (!empty($options)) {
    $form['field_attach_form_selective'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add fields to form'),
      '#collapsable' => FALSE,
      '#collapsed' => FALSE,
      '#weight' => -9999,
    );
    $form['field_attach_form_selective']['field_attach_form_selective_field_selector'] = array(
      '#type' => 'select',
      '#title' => t('Add fields to form'),
      '#title_display' => 'invisible',
      '#description' => t('Multiple options may be selected by holding <em>&lt;cmd&gt;</em>, <em>&lt;ctrl&gt;</em>, or <em>&lt;shift&gt;</em> when clicking.'),
      '#options' => $options,
      '#multiple' => TRUE,
      '#size' => (count($options) > 50) ? 20 : 10,
    );
    $form['field_attach_form_selective']['field_attach_form_selective_field_selector_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update form'),
      '#limit_validation_errors' => array(
        array('field_attach_form_selective_field_selector'),
        array('field_attach_form_selective_field_selector_submit'),
      ),
      '#submit' => array('_field_attach_form_selective_field_selector_submit_update_form'),
    );
  }
}

/**
 * Add the field selector widget to a form.
 */
function _field_attach_form_selective_field_selector_submit_update_form(&$form, &$form_state) {
  // Support field widgets which choose multiple values, or a single value.
  $fields_to_add = $form_state['values']['field_attach_form_selective_field_selector'];
  if (!is_array($fields_to_add)) {
    $fields_to_add = array($fields_to_add);
  }
  $fields_to_add = drupal_map_assoc($fields_to_add);
  if (!isset($form_state['field_attach_form_selective_fields'])) {
    $form_state['field_attach_form_selective_fields'] = array();
  }
  $form_state['field_attach_form_selective_fields'] += $fields_to_add;
  $form_state['rebuild'] = TRUE;
}
